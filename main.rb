require 'prometheus/api_client'
require 'statusio'

statusio_metric_id = ENV.fetch('STATUSIO_METRIC_ID', nil) || ( raise "Please set STATUSIO_METRIC_ID" )
statusio_page_id = ENV.fetch('STATUSIO_PAGE_ID', nil) || ( raise "Please set STATUSIO_PAGE_ID" )

statusio_api_id = ENV.fetch('STATUSIO_API_ID', nil) || ( raise "Please set STATUSIO_API_ID" )
statusio_api_key = ENV.fetch('STATUSIO_API_KEY', nil) || ( raise "Please set STATUSIO_API_KEY" )

# fixes: warning: Overriding "Content-Type" header "application/json" with "application/x-www-form-urlencoded" due to payload
class FixedStatusioClient < StatusioClient
  private def request(params)
    params[:payload] = JSON.dump(params[:payload]) if params.key?(:payload)
    super
  end
end

body = {
  day_avg: 0,
  day_start: 0,
  day_dates: [],
  day_values: [],
  week_avg: 0,
  week_start: 0,
  week_dates: [],
  week_values: [],
  month_avg: 0,
  month_start: 0,
  month_dates: [],
  month_values: []
}

now = Time.now.utc
day_start = (now - 24*3600)
week_start = (now - 7*24*3600)
month_start = (now - 30*24*3600)

body[:day_start] = day_start.to_time.to_i
body[:week_start] = week_start.to_time.to_i
body[:month_start] = month_start.to_time.to_i

prometheus = Prometheus::ApiClient.client(url: 'http://thanos-query-frontend-internal.ops.gke.gitlab.net:9090', options: { timeout: 30 } )

result = prometheus.query_range(
  query: 'min_over_time(gitlab_service_apdex:ratio{environment="gprd",tier="runners",type="ci-runners",stage="main"}[5m])',
  start: day_start.strftime('%Y-%m-%dT%H:%M:%SZ'),
  end:   now.strftime('%Y-%m-%dT%H:%M:%SZ'),
  step:  '3600s',
)
result["result"][0]["values"].each do |value|
  body[:day_dates] << Time.at(value[0])
  body[:day_values] << (value[1].to_f * 100).round(3)
end
body[:day_avg] = (body[:day_values].sum(0.0) / body[:day_values].size).round(3)

result = prometheus.query_range(
  query: 'min_over_time(gitlab_service_apdex:ratio{environment="gprd",tier="runners",type="ci-runners",stage="main"}[15m])',
  start: week_start.strftime('%Y-%m-%dT%H:%M:%SZ'),
  end:   now.strftime('%Y-%m-%dT%H:%M:%SZ'),
  step:  '86400s', # 24h
)
result["result"][0]["values"].each do |value|
  body[:week_dates] << Time.at(value[0])
  body[:week_values] << (value[1].to_f * 100).round(3)
end
body[:week_avg] = (body[:week_values].sum(0.0) / body[:week_values].size).round(3)

result = prometheus.query_range(
  query: 'min_over_time(gitlab_service_apdex:ratio{environment="gprd",tier="runners",type="ci-runners",stage="main"}[30m])',
  start: month_start.strftime('%Y-%m-%dT%H:%M:%SZ'),
  end:   now.strftime('%Y-%m-%dT%H:%M:%SZ'),
  step:  '86400s', # 24h
)
result["result"][0]["values"].each do |value|
  body[:month_dates] << Time.at(value[0])
  body[:month_values] << (value[1].to_f * 100).round(3)
end
body[:month_avg] = (body[:month_values].sum(0.0) / body[:month_values].size).round(3)

statusio_client = FixedStatusioClient.new(statusio_api_key, statusio_api_id)
statusio_client.metric_update(
  statusio_page_id,
  statusio_metric_id,
  *body.values
)
